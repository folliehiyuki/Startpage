# A minimal startpage

> The site is hosted on Gitlab --> [here](https://folliehiyuki.gitlab.io/Startpage/)

### :clipboard: To-do list :clipboard:

- [ ] Make the page responsive

### :star2: Credits :star2:

Fonts used:

- [FiraCode Nerd Font](https://github.com/ryanoasis/nerd-fonts) (for the glyphs)
- [Sarasa Gothic](https://github.com/be5invis/Sarasa-Gothic) (Japanese supported)

The Firefox CSS is taken from [here](https://github.com/evanswa0606/firefox-simpletheme) with some minor changes.
